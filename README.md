# eBuzzz adapter, an eBus to serial adapter

An eBus adapter, to convert eBus signal levels to serial level for a serial to USB converter (Works with CP2102)

# Software

The eBus protocol can be connected to with [ebusd](https://github.com/john30/ebusd). If you happen to use Home Assistant you can also connect ebusd to your Home Assistant.
